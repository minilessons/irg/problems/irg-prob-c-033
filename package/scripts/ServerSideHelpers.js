
const sign = Math.sign;

function copy(src) {
	const dst = {};
	Object.keys(src).forEach(function(k) { dst[k] = src[k]; });
	return dst;
}

function range(n) {
	return Array.apply(null, Array(n)).map(function (_, i) { return i; });
}

function doubleEquals(fst, snd, epsilon) { return Math.abs(fst - snd) < epsilon; }
const deq = doubleEquals;

function point(x, y) { return new THREE.Vector3(x, y, 1); }
function line(pt1, pt2) { return [pt1, pt2]; }
function lineStart(l) { return l[0]; }
function lineEnd(l) { return l[1]; }
function randomSign() { return sign(Math.random() - 0.5); }
function random(limit) { return Math.round(Math.random() * limit); }

function randomFromRange(lo, hi) {
	const range = hi - lo;
	return lo + random(range);
}

function randomPoint(limit) {
	return point(randomSign() * random(limit), 
							 randomSign() * random(limit));
}

function distanceSq(vector) {
	return vector.x * vector.x + vector.y * vector.y + vector.z * vector.z;
}

function pointEquals(pt1, pt2) {
	if (!pt1) return !pt2;
	if (!pt2) return !pt1;

	const epsilon = 0.001;
	return deq(pt1.x, pt2.x, epsilon) && deq(pt1.y, pt2.y, epsilon);
}

function lineEquals(l1, l2, epsilon) {
	const startStart = pointEquals(lineStart(l1), lineStart(l2), epsilon)
									&& pointEquals(lineEnd(l1), lineEnd(l2), epsilon);
	
	const startEnd = pointEquals(lineStart(l1), lineEnd(l2), epsilon)
								&& pointEquals(lineEnd(l1), lineStart(l2), epsilon);

	return startStart || startEnd;
}
