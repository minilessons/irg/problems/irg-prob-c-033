
/*@#common-include#(three.js)*/
/*@#include#(ServerSideHelpers.js)*/

function calculateSolution(pts, t) {
	const ret = {};
	ret.lines = [];

	var lines = [];
	for (var i = 0; i < pts.length - 1; i++)
		lines.push(line(pts[i], pts[i + 1]));

	function calcMidpoint(pt1, pt2, t) {
		return point(pt1.x + (-pt1.x + pt2.x) * t, pt1.y + (-pt1.y + pt2.y) * t);
	}

	while (true) {
		const midpoints = [];
		const newLines = [];

		midpoints.push(calcMidpoint(lines[0][0], lines[0][1], t));
		for (var i = 1; i < lines.length; i++) {
			const currLine = lines[i];
			const midpoint = calcMidpoint(currLine[0], currLine[1], t);

			midpoints.push(midpoint);
			newLines.push(line(midpoints[i-1], midpoints[i]));
		}

		if (newLines.length == 0) {
			ret.finalPoint = midpoints[0];
			break;
		}

		ret.lines = ret.lines.concat(newLines);
		lines = newLines;
	}

	return ret;
}

function generateRandomPoints(nPoints, minScale, maxScale) {
	if (nPoints > 4) {
		throw 'Random generator expects max 4 points';
	}

	if (minScale > maxScale) {
		throw 'Min scale should be at most equal to max scale';
	}

	const randomLimit = 2;
	const step = (randomLimit * 2) / nPoints;
	const limits = {
		minX: -randomLimit, rangeX: 0.5,
		minY: 1.0, rangeY: 0.5
	};

	const rotateAngle = randomFromRange(-Math.PI / 3, Math.PI / 3);
	const scale = randomFromRange(minScale, maxScale);
	const flip = Math.sign(Math.random() - 0.5);
	const zAxis = new THREE.Vector3(0, 0, 1);

	return range(nPoints).map(function(i) {
		const x = randomFromRange(limits.minX, limits.minX + limits.rangeX);
		const y = randomFromRange(limits.minY, limits.minY + limits.rangeY);

		switch (i) {
			case 0: limits.minY = -randomLimit; break; 
			case nPoints - 2: limits.minY = randomLimit; break;
		}

		limits.minX += step;

		return new THREE.Vector3(x, y, 1);
	}).map(function(pt) {
		return pt.multiplyScalar(scale).setZ(1);
	}).map(function(pt) {
		return pt.setY(pt.y * flip);
	}).map(function(pt) {
		return pt.applyAxisAngle(zAxis, rotateAngle);
	});
}

function getMaxValueOnAnyAxis(points) {
	return points.map(function(pt) { return Math.max(Math.abs(pt.x), Math.abs(pt.y)); })
		.reduce(function(a, b) { return Math.max(a, b); });
}

// Ova je metoda namijenjena inicijalizaciji zadatka.
// Dobiva questionConfigData koji postavlja editor konfiguracije
// -------------------------------------------------------------------------------------------------
function questionInitialize(questionConfig) {
	const maxIntersectPoints = questionConfig.maxIntersectPoints;
	
	if (maxIntersectPoints > 10 || maxIntersectPoints <= 1) {
		throw 'Question should have [2, 10] intersect points';
	}

	const numPoints = questionConfig.numPoints;
	const minScale = questionConfig.minScale;
	const maxScale = questionConfig.maxScale;

	const pts = generateRandomPoints(numPoints, minScale, maxScale);
	const delta = 4;
	const maxAxis = Math.ceil(getMaxValueOnAnyAxis(pts)) + delta;
	const tDenominator = randomFromRange(2, maxIntersectPoints);
	const tNumerator = randomFromRange(1, tDenominator - 1);

	userData.question = {
		points: pts,
		tDenominator: tDenominator,
		tNumerator: tNumerator,
		maxAxis: maxAxis,
		width: questionConfig.canvasWidth,
		height: questionConfig.canvasHeight
	};

	const t = tNumerator / tDenominator;
	const solution = calculateSolution(pts, t);

	userData.question.correct = {
		lines: solution.lines,
		selectedPoint: solution.finalPoint 
	};
}

// Ova je metoda namijenjena generiranju varijabli koje se dinamički računaju i nigdje ne pohranjuju.
// Ove varijable bit će vidljive kodu koji HTML-predloške odnosno ServerSide-JavaScript-predloške
// lokalizira.
// -------------------------------------------------------------------------------------------------
function getComputedProperties() {
	function toDisplayString(pt) { return "(" + pt.x.toFixed(2) + ", " + pt.y.toFixed(2) + ")"; }
  return { tDenominator: userData.question.tDenominator, 
					 tNumerator: userData.question.tNumerator,
					 startingPoint: toDisplayString(userData.question.points[0]) };
}


// Ova metoda poziva se kako bi se obavilo vrednovanje zadatka. Pozivatelju treba vratiti objekt:
// {correctness: X, solved: Y}, gdje su X in [0,1], Y in {true,false}.
// -------------------------------------------------------------------------------------------------
function questionEvaluate() {

	const epsilon = 0.001;

	function determineLineCorrectness(userLines, expectedLines) {
		const matchedLines = expectedLines.map(function() { return false; });
		const totalExpected = expectedLines.length;
		var totalFound = 0;

		while (userLines && userLines.length > 0) {
			const currentUserLine = userLines.pop();
			var correctLine = false;

			for(var j = 0; j < expectedLines.length; j++) {
				if (matchedLines[j]) continue;

				correctLine = lineEquals(currentUserLine, expectedLines[j], epsilon);

				if (correctLine) {
					matchedLines[j] = true;
					break;
				}
			}

			// ako postoji linija koju je korisnik napravio, ali nije
			// tocna => oduzmi jedan bod 
			if (correctLine) totalFound++;
			else totalFound--;
		}

		return totalFound / totalExpected;
	}

	function determineCorrectness() {
		const userLines = Object.keys(userData.questionState.connections)
			.map(function(key) { return userData.questionState.connections[key] })
			.map(function(conn) {
				const fstUuid = conn[0];
				const sndUuid = conn[1];

				return line(userData.questionState.positions[fstUuid],
						 		 		userData.questionState.positions[sndUuid]);
			});

		const lineCorrectness = Math.max(determineLineCorrectness(userLines, userData.question.correct.lines), 0);
		const userSelectedPointId = userData.questionState.selectedPoint;
		const pointCorrectness = pointEquals(userData.questionState.positions[userSelectedPointId], 
																				 userData.question.correct.selectedPoint,
																				 epsilon);

		return (lineCorrectness + pointCorrectness) / 2.0;
	}
	
	const tNumerator = userData.question.tNumerator;
	const tDenominator = userData.question.tDenominator;
	const startingPoints = userData.question.points;
	userData.correctQuestionState = emulateCorrectState(startingPoints, tNumerator, tDenominator);
	userData.isSubmitted = true;

  return { correctness: determineCorrectness(), 
					 solved: Object.keys(userData.questionState.positions).length > userData.questionState.startingUuids.length };
}

// stvori objekt koji ObjectManager.js zna procitati
// koristi se za prikaz tocnog stanja client-side
function emulateCorrectState(startPoints, tnum, tdenom) {
	var dummyUuid = 0;

	const ret = {};
	ret.connections = {};
	ret.lineUuidToNumDivisions = {};
	ret.startingUuids = [];

	var lines = [];
	const numPoints = startPoints.length;

	for (var i = 0; i < numPoints - 1; i++, dummyUuid++) {
		ret.startingUuids.push(dummyUuid);
		lines.push(dummyUuid);
		ret.lineUuidToNumDivisions[dummyUuid] = tdenom;
	}
	
	ret.startingUuids = ret.startingUuids.concat(startPoints.map(function() { return dummyUuid++; }));
	ret.lineUuidToPointUuids = {};

	function calcMidpoint(lineUuid) {
		var midpointUuid = -1;
		ret.lineUuidToPointUuids[lineUuid] = [];
		for (var i = 1; i < tdenom; i++, dummyUuid++) {
			if (i == tnum) midpointUuid = dummyUuid;
			ret.lineUuidToPointUuids[lineUuid].push(dummyUuid);
		}

		ret.lineUuidToNumDivisions[lineUuid] = tdenom;
		return midpointUuid;
	}

	while (true) {
		const newLines = [];
		const midpoints = [];

		midpoints.push(calcMidpoint(lines[0]));
		for (var i = 1; i < lines.length; i++, dummyUuid++) {
			const currLine = lines[i];
			const midpoint = calcMidpoint(currLine);
			midpoints.push(midpoint);

			ret.connections[dummyUuid] = line(midpoints[i-1], midpoints[i]);
			newLines.push(dummyUuid);
		}

		if (newLines.length == 0) {
			ret.selectedPoint = midpoints[0];
			break;
		}

		lines = newLines;
	}

	return ret;
}

// Metoda koja se poziva kako bi izvadila zajednički dio stanja zadatka koje je potrebno za prikaz zadatka.
// -------------------------------------------------------------------------------------------------
function exportCommonState() {
	const common = copy(userData.question);
	common.correct = undefined;

	return common;
}

// Metoda koja se poziva kako bi izvadilo stanje zadatka koje je postavio korisnik (njegovo rješenje).
// Razlikuje se svrha prikaza: korisnikovo rješenje ili točno rješenje
// -------------------------------------------------------------------------------------------------
function exportUserState(stateType) {
  if (stateType=="USER") {
    return { qs: userData.questionState, disabled: userData.isSubmitted };
  }

  if (stateType=="CORRECT") {
    return { qs: userData.correctQuestionState, disabled: true };
  }

  return {};
}

// Metoda koja se poziva kako bi vratila stanje zadatka koje odgovara nerješavanom zadatku.
// -------------------------------------------------------------------------------------------------
function exportEmptyState() {
  return { };
}

// Metoda koja se poziva kako bi pohranila stanje zadatka na temelju onoga što je korisnik napravio.
// -------------------------------------------------------------------------------------------------
function importQuestionState(data) {
  userData.questionState = data;
}
