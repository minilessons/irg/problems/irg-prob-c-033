class DeCasteljauSelection {
    constructor(objectsProvider, 
                scene, 
                intersectionFinder, 
                objectManager,
								enabled) {
        this.selectedLine = undefined;
        this.composites = scene.children;
        this.intersectionFinder = intersectionFinder;
        this.scene = scene;

        this.onMousemove = this.onMousemove.bind(this);
        this.onMousedown = this.onMousedown.bind(this);
        this.onKeyup = this.onKeyup.bind(this);
        this.trySelectFirstPoint = this.trySelectFirstPoint.bind(this);

        this.objectsProvider = objectsProvider;

        this.objectManager = objectManager;
				this.enabled = enabled;
    }

    _findClosestPoint(intersections, mouseWorld) {
        var intersect = undefined;
        var intersectD = undefined;

        for (let i in intersections) {
            const currIntersection = intersections[i];
            const distance = mouseWorld.distanceTo(currIntersection.point);

            if (!intersectD || distance < intersectD) {
                intersect = currIntersection.object;
                intersectD = distance;
            }
        }
        return intersect;
    }

    onMousemove(e) {
				if (!this.enabled) return;

        this.selectedLine = makeUnselected(this.selectedLine);

        const mouseWorld = this.intersectionFinder.worldCoordinates(e.clientX, e.clientY);
        const intersections = this.intersectionFinder.getIntersections(e.clientX, e.clientY, this.objectsProvider().filter(isLine));
        const closestIntersection = this._findClosestPoint(intersections, mouseWorld);

        this.selectedLine = makeSelected(closestIntersection);
    }

    trySelectFirstPoint(e) {
        if (e.which != LEFT_MOUSE_BUTTON) return;
        const allObjects = this.objectsProvider();
        const points = allObjects.filter(isPoint);

        const found = this.intersectionFinder.getIntersections(e.clientX, e.clientY, points)
            .map(o => o.object)
            .find(o => o);

        if (!found) {
            this.objectManager.selectedPoint = makeUnselected(this.objectManager.selectedPoint);
            return;
        }

        this.objectManager.selectedPoint = makeSelected(found);
    }

    onMousedown(e) {
				if (!this.enabled) return;

        if (!this.objectManager.selectedPoint) return this.trySelectFirstPoint(e);
        if (e.which != LEFT_MOUSE_BUTTON) return;

        const allObjects = this.objectsProvider();
        const points = allObjects.filter(isPoint);

        const selectedPoint2 = this.intersectionFinder.getIntersections(e.clientX, e.clientY, points)
                .map(o => o.object)
                .find(o => o);

        const selectedPoint1 = this.objectManager.selectedPoint;
        this.objectManager.selectedPoint = makeUnselected(this.objectManager.selectedPoint);
        
        if (selectedPoint2) {
            this.objectManager.createConnection(selectedPoint1, selectedPoint2);
        }
    }

    onKeyup(keyEvent) {
				if (!this.enabled) return;

        const key = keyEvent.key;
        var keyAsInteger = parseInt(key);

				if (keyAsInteger == 0) keyAsInteger = 10;
        if (!this.selectedLine) return;

        this.objectManager.selectedPoint = makeUnselected(this.objectManager.selectedPoint);
        this.objectManager.createDivisionsForLine(this.selectedLine, keyAsInteger);
    }

}
