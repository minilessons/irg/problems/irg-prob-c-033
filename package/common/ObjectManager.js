
function getOrDefault(o, key, defaultItem) { 
    const f = o[key];
    return f || defaultItem;
}

function merge(o, key, defaultItem, method) {
    const existing = o[key];
    o[key] = method(existing || defaultItem);
    return o[key];
}

class ObjectManager {
    constructor(scene, startingPoints, startingUuids) {
        this.scene = scene;

        this.startingPoints = startingPoints;
        this.startingUuids = startingUuids;

        this.reset = this.reset.bind(this);

        this.createDivisionsForLine = this.createDivisionsForLine.bind(this);
        this.createConnection = this.createConnection.bind(this);
        
        this.toJSON = this.toJSON.bind(this);
        this.loadFromJSON = this.loadFromJSON.bind(this);

        this._removeLine = this._removeLine.bind(this);
        this._removePoint = this._removePoint.bind(this);

        this.reset();
    }

    reset() {
        this.lineUuidToChildLines = {};
        this.lineUuidToChildPoints = {};

        this.lineUuidToParentLines = {};
        this.pointUuidToParentLines = {};
        this.pointUuidToConnectedLines = {};

        this.lineUuidToNumDivisions = {}

        this.connections = {};
        this.selectedPoint = null;
    }

    _removeLine(line) {
        const uuid = line.uuid;
        const parents = this.lineUuidToParentLines[uuid];
        delete this.lineUuidToParentLines[uuid];

        if (!parents) 
            return;

				delete this.lineUuidToChildLines[parent];

        const childPoints = this.lineUuidToChildPoints[uuid] || [];
        childPoints.forEach(this._removePoint);

        delete this.lineUuidToChildPoints[uuid];
        delete this.connections[uuid];
        delete this.lineUuidToNumDivisions[uuid];

        this.scene.remove(line);
    }

    _removePoint(point) {
        const uuid = point.uuid;
        const parentLines = this.pointUuidToParentLines[uuid];

        delete this.pointUuidToParentLines[uuid];

        if (!parentLines) 
            return;
        
        getOrDefault(this.pointUuidToConnectedLines, uuid, []).forEach(this._removeLine);
        delete this.pointUuidToConnectedLines[uuid];
        this.scene.remove(point);
    }

    createDivisionsForLine(line, numDivisions) {
        const newPoints = createDivisions(line, numDivisions).map(markSelectable).map(markUserCreated);

        const existingPoints = getOrDefault(this.lineUuidToChildPoints, line.uuid, []);
        existingPoints.forEach(this._removePoint);

        const existingLines = getOrDefault(this.lineUuidToChildLines, line.uuid, []);
        existingLines.forEach(this._removeLine);

        this.lineUuidToChildPoints[line.uuid] = newPoints;
        this.lineUuidToNumDivisions[line.uuid] = numDivisions;
        delete this.lineUuidToChildLines[line.uuid];
        
        newPoints.forEach(point => {
            this.pointUuidToParentLines[point.uuid] = [line];
            this.scene.add(point);
        });

        return newPoints;
    }

    createConnection(point1, point2) {
        if (point1 === point2) {
            return;
        }

        const uuid1 = point1.uuid;
        const uuid2 = point2.uuid;

        const pos1 = pointLocation(point1);
        const pos2 = pointLocation(point2);
        const newLine = markUserCreated( markSelectable( styledLine(pos1, pos2) ) );

        const parentLine1 = this.pointUuidToParentLines[uuid1] [0];
        const parentLine2 = this.pointUuidToParentLines[uuid2] [0];
        merge(this.lineUuidToChildLines, parentLine1.uuid, [], l => l.concat([newLine]) );
        merge(this.lineUuidToChildLines, parentLine2.uuid, [], l => l.concat([newLine]) );
        merge(this.pointUuidToConnectedLines, point1.uuid, [], l => l.concat([newLine]) );
        merge(this.pointUuidToConnectedLines, point2.uuid, [], l => l.concat([newLine]) );

        this.lineUuidToParentLines[newLine.uuid] = [parentLine1.uuid, parentLine2.uuid];
        this.connections[newLine.uuid] = [point1.uuid, point2.uuid];
				
        this.scene.add(newLine);
        return newLine;
    }

    toJSON() {
        const pointUuids = {};
        Object.keys(this.lineUuidToChildPoints)
					.forEach(k => pointUuids[k] = this.lineUuidToChildPoints[k].map(pt => pt.uuid));
            
        const selectedPointUuid = this.selectedPoint ? this.selectedPoint.uuid : undefined;

				function mapToPositions(objects) {
					const result = {};
					for (let o of objects) {
						const uuid = o.uuid;
						
						if (!isSelectable(o)) 
							continue;

						const sphere = o.geometry.boundingSphere;
						result[uuid] = (sphere && sphere.center);
					}

					return result;
				}

        return { lineUuidToPointUuids: pointUuids,
                 startingUuids: this.startingUuids,
                 lineUuidToNumDivisions:this.lineUuidToNumDivisions,
                 connections: this.connections,
                 selectedPoint: selectedPointUuid,
								 positions: mapToPositions(this.scene.children) };
    }

    loadFromJSON(json, linesAndPoints) {
        const mappingsFromPreviousToCurrent = {};
        const uuidToInstance = {};
        
        const mapOldUuids = (newInstances, oldUuids) => {
            for (var i in oldUuids) {
                const previousUuid = oldUuids[i];
                const newUuid = newInstances[i].uuid;

                mappingsFromPreviousToCurrent[previousUuid] = newUuid;
                uuidToInstance[newUuid] = newInstances[i];
            }
        };

        mapOldUuids(linesAndPoints, json.startingUuids);
        
        Object.keys(json.lineUuidToNumDivisions)
            .forEach(previousUuid => {
                const numDivisions = json.lineUuidToNumDivisions[previousUuid];
                var currentUuid = mappingsFromPreviousToCurrent[previousUuid];
                var currentLine = uuidToInstance[currentUuid];

                if (!currentLine) {
                    const conn = json.connections[previousUuid];
                    const newPointUuids = [ mappingsFromPreviousToCurrent[conn[0]], mappingsFromPreviousToCurrent[conn[1]] ];

                    currentLine = this.createConnection(uuidToInstance[newPointUuids[0]], uuidToInstance[newPointUuids[1]]);
                    currentUuid = currentLine.uuid;
                    
                    mappingsFromPreviousToCurrent[previousUuid] = currentUuid;
                    uuidToInstance[currentUuid] = currentLine;
                }

                const newDivisions = this.createDivisionsForLine(currentLine, numDivisions);
                mapOldUuids(newDivisions, json.lineUuidToPointUuids[previousUuid]);
            });

        Object.keys(json.connections)
            .filter(lineUuid => !mappingsFromPreviousToCurrent[lineUuid])
            .map(oldUuid => json.connections[oldUuid])
            .map(conns => conns.map(connUuid => mappingsFromPreviousToCurrent[connUuid]))
            .map(newUuids => newUuids.map(newUuid => uuidToInstance[newUuid]))
            .forEach(points => this.createConnection(points[0], points[1]));

        const selectedPointUuid = mappingsFromPreviousToCurrent[json.selectedPoint];
        this.selectedPoint = makeSelected(uuidToInstance[selectedPointUuid]);
    }

}
