
function markSelectable(obj) {
    obj.userData.isSelectable = true;
    return obj;
}

function isSelectable(obj) {
    return obj.userData.isSelectable;
}

function styledLine(A, B) {
    return line(A, B, { color: LINE_COLOR_UNSELECTED, linewidth: 3 });
}

function styledPoint(A) {
    return point(A, { color: POINT_COLOR_UNSELECTED });
}

function getEndpoints(line) {
    return line.geometry.vertices.map(vertex => vertex.clone());
}

function createDivisions(selected, numDivisions) {
    const composite = [];
    const endpoints = getEndpoints(selected);
    const startPt = endpoints[0].clone();
    
    const dx = (endpoints[1].x - endpoints[0].x) / numDivisions;
    const dy = (endpoints[1].y - endpoints[0].y) / numDivisions;

    for (var i = 0; i < numDivisions - 1; i++) {
        startPt.x += dx;
        startPt.y += dy;

        const newPoint = styledPoint(startPt.clone());
        markSelectable(newPoint);

        composite.push(newPoint);
    }

    return composite;
}

function markUserCreated(obj) {
    obj.userData.isUserCreated = true;
    return obj;
}

function isUserCreated(obj) {
    return obj.userData.isUserCreated;
}

function toLines(points) {
    return Array(points.length - 1).fill()
        .map((_, i) => styledLine(points[i], points[i + 1]))
        .map(line => markSelectable(line));
}

function constructDeCasteljau(points) {
    const graphicPoints = points
        .map(pt => styledPoint(pt))
        .map(pt => markSelectable(pt));
    const graphicLines = toLines(points);

    return graphicLines.concat(graphicPoints);
}

function makeSelected(obj) {
    if (!obj) return obj;
    const color = COLORS_BY_TYPE[obj.type].selected;

    obj.material.color = new THREE.Color(color);
    obj.material.needsUpdate = true;

    return obj;
}

function makeUnselected(obj) {
    if (!obj) return obj;
    const color = COLORS_BY_TYPE[obj.type].unselected;

    obj.material.color = new THREE.Color(color);
    obj.material.needsUpdate = true;
    
    return undefined;
}


