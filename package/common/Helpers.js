
function vec2(x, y) {
    return new THREE.Vector2(x, y);
}

function vec3(x, y, z) {
    return new THREE.Vector3(x, y, z);
}

function toVec3(vec2) {
    return new THREE.Vector3(vec2.x, vec2.y, 0);
}

function toVec2(vec3) {
    return vec2(vec3.x, vec3.y);
}

function pushAsVec3(geometry, vec2) {
    geometry.vertices.push(toVec3(vec2));
}

function pointLocation(point) {
    if (!point) return;

    if (!point.geometry.boundingSphere)
        point.geometry.computeBoundingSphere();

    return point.geometry.boundingSphere.center.clone();
}

function isPoint(obj) {
	return obj.userData.isPoint;
}

function point(loc, materialProperties) {
    const RADIUS = 0.20;
    const N_SEGMENTS = 10;

    const material = new THREE.MeshBasicMaterial(materialProperties);
    const geometry = new THREE.CircleGeometry(RADIUS, N_SEGMENTS);

    geometry.translate(loc.x, loc.y, DEFAULT_Z_2D);
    geometry.computeFaceNormals();
    
    const pt = new THREE.Mesh(geometry, material);
		pt.userData.isPoint = true;

		return pt;
}

function isLine(obj) {
	return obj.userData.isLine;
}

function line(A, B, materialProperties) {
    const geometry = new THREE.Geometry();
    const material = new THREE.LineBasicMaterial(materialProperties);

    pushAsVec3(geometry, A);
    pushAsVec3(geometry, B);

    const obj = new THREE.LineSegments(geometry, material);
		obj.userData.isLine = true;

		return obj;
}

