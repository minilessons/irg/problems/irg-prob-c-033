
/*@#common-include#(three.js)*/
/*@#common-include#(Constants.js)*/
/*@#common-include#(Helpers.js)*/
/*@#common-include#(IntersectionFinder.js)*/
/*@#common-include#(HelperLinesHandler.js)*/
/*@#common-include#(ScreenMovementHandler.js)*/
/*@#common-include#(Display2D.js)*/
/*@#common-include#(DeCasteljau.js)*/
/*@#common-include#(ObjectManager.js)*/
/*@#common-include#(DeCasteljauSelection.js)*/

/**
 *	cst:
 *		points		:: [vec2] - tocke koje definiraju segmente preko kojih radimo postupak
 *	st:
 *		disabled	:: boolean - je li korisnik submittao zadatak (ako da, ne dozvoli promjene)
 *		qs				:: custom (vidi ObjectManager) - zadnje spremljeno stanje
 *								 objekt je malo glupav jer drzi u vise mapa informacije o geometrijskim objektima (UUID / pozicija)
 *								 no ThreeJS je imao problema sa serijalizacijom kada sam spremao podatke u JS objekte/klase 
 *								 pa je ovo next best thing
 */
var api = {

	initQuestion: function(cst, st) {
		const wrapper = document.getElementById(questionLocalID + "_wrap");
		const dimensionProvider = () => Object.assign({ width: cst.canvasWidth || 500, 
																										height: cst.canvasHeight || 500 });
		const wrapperDimensions = dimensionProvider();
		const renderer = getRenderer(wrapperDimensions);
		const scene = new THREE.Scene();
		const camera = default2DCamera(PIXELS_PER_UNIT, wrapperDimensions);
		const grid = construct2DGrid(cst.maxAxis, CENTER_LINE_COLOR, GRID_COLOR);
		const snapToGrid = false;

		wrapper.appendChild(renderer.domElement);
		scene.add(grid);

		const movementHandler = new ScreenMovementHandler(renderer, camera, RIGHT_MOUSE_BUTTON);
		wrapper.addEventListener('mousedown', movementHandler.onMousedown);
		wrapper.addEventListener('mouseup', movementHandler.onMouseup);
		wrapper.addEventListener('mousemove', movementHandler.onMousemove);
		wrapper.addEventListener('wheel', movementHandler.onMousewheel);
		wrapper.oncontextmenu = e => e.preventDefault();
		wrapper.onwheel = e => e.preventDefault();

		const intersectionFinder = new IntersectionFinder(camera, 
			() => renderer.context.canvas.getBoundingClientRect(), new THREE.Raycaster());
		
		const helperLinesHandler = new HelperLinesHandler(snapToGrid, grid, intersectionFinder, 
			wrapper, scene, cst.maxAxis);
		wrapper.addEventListener('mousemove', helperLinesHandler.onMousemove);
		wrapper.appendChild(helperLinesHandler.positionDataElement);

		const composites = constructDeCasteljau(cst.points.map(xy => vec2(xy.x, xy.y)));
		composites.forEach(composite => scene.add(composite));

		const objectManager = new ObjectManager(scene, cst.points, composites.map(c => c.uuid));
		const casteljauSelection = new DeCasteljauSelection(() => scene.children.filter(isSelectable), 
			scene, intersectionFinder, objectManager, !st.disabled);

		wrapper.addEventListener('mousemove', casteljauSelection.onMousemove, false);
		wrapper.addEventListener('mousedown', casteljauSelection.onMousedown, false);
		document.addEventListener('keyup', casteljauSelection.onKeyup);

		window.addEventListener('resize', 
			onWindowResize(PIXELS_PER_UNIT, dimensionProvider, intersectionFinder, camera, renderer), 
			false);

		function animate() {
			requestAnimationFrame(animate);
			renderer.render(scene, camera);
		}

		animate();		

		if (st.qs) {
			objectManager.loadFromJSON(st.qs, composites);
		}

		this.takeState = () => objectManager.toJSON();

		this.resetState = (cst, st) => { 
			scene.children = helperLinesHandler.axes.concat([grid]).concat(composites);
			objectManager.reset();
		 };

		this.revertState = (cst, st) => {
			scene.children = helperLinesHandler.axes.concat([grid]).concat(composites);
			objectManager.reset();

			if(st.qs) {
				objectManager.loadFromJSON(st.qs, composites);
			}
		};
	}
};

return api;

